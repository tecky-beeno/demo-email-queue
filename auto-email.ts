import { createTransport } from "nodemailer";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

let transport = createTransport({
  service: "google",
});

let lock: Promise<any> = Promise.resolve();

export async function sendEmail(options: {
  subject: string;
  contentHTML: string;
  address: string;
}) {
  let promise = lock
    .then(() => sleep(2000))
    .then(() =>
      transport.sendMail({
        from: "info@mydomain.com",
        to: options.address,
        subject: options.subject,
        html: options.contentHTML,
      })
    );
  lock = promise.catch((error) => {
    console.log("failed to send email", { options, error });
    // catch and return, otherwise one failed attempt will block all future attempts
    return Promise.resolve();
  });
  return promise;
}

let testMails = [
  { subject: "Hi", contentHTML: "How are you?", address: "alice@gmail.com" },
  {
    subject: "re^: Hi",
    contentHTML: "I am good, you?",
    address: "bob@gmail.com",
  },
  {
    subject: "re^2: Hi",
    contentHTML: "I am also good, are we blocked?",
    address: "alice@gmail.com",
  },
];

function mockTransport() {
  transport.sendMail = async function (...args: any[]) {
    console.log("sendMail:", args);
    console.log("sleep 1 second to fake networking delay");
    await sleep(1000);
    console.log("return fake result");
    return "ok";
  };
}

async function testWithMap() {
  console.log("test auto email with array.map or array.forEach");

  mockTransport();

  let ps = testMails.map((mail) => sendEmail(mail));
  let p = Promise.all(ps);
  await p;
  console.log("all sent");
}

async function testWithLoop() {
  console.log("test auto email with loop");

  mockTransport();
  for (let mail of testMails) {
    try {
      await sendEmail(mail);
    } catch (error) {
      console.log("failed on one email", { mail, error });
    }
  }
}

testWithMap();
// testWithLoop()
